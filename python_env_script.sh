#!/bin/bash

echo "UPDATING SYSTEM FILES"
sudo apt update
sudo apt -y upgrade

echo "INSTALLING GIT AND CURL"
sudo apt install -y curl
sudo apt install -y git

echo "DOWNLOADING ANACONDA3"
cd /tmp
curl -O https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh

echo "INSTALLING ANACONDA3"
sudo bash Anaconda3-2020.02-Linux-x86_64.sh -b -p /opt/anaconda3/2020-02/
source ~/.bashrc
echo "export PATH=/opt/anaconda3/2020-02/bin:$PATH" >> ~/.bashrc
echo "export PATH=/opt/anaconda3/2020-02/bin:$PATH" >> ~/.profile

echo "CLONING IRIS-F1 USER SCRIPTS"
cd ~
git clone https://bitbucket.org/irsweep/iris-f1userscripts.git
cd iris-f1userscripts
git checkout master
git pull

cd ~/pythonenvironmentsetup
chmod a+x startServer.sh

echo "ADDING PYTHONPATH"
echo "export PYTHONPATH=$PYTHONPATH:$HOME/iris-f1userscripts/python_scripts/heterodyne_postprocessing" >> ~/.bashrc
echo "export PYTHONPATH=$PYTHONPATH:$HOME/iris-f1userscripts/python_scripts/heterodyne_postprocessing" >> ~/.profile

echo "ADD SPYDER TO SEARCHABLE APPLICATIONS"
cd ~/.local/share/applications
echo "[Desktop Entry]
Name=Spyder
Version=3.0
Type=Application
Exec=/opt/anaconda3/2020-02/bin/spyder
Icon=/opt/anaconda3/2020-02/lib/python3.7/site-packages/spyder/images/spyder_dark.png
Comment=Open Spyder
Terminal=false" > spyder.desktop
chmod a+x spyder.desktop

echo "ADD START SERVER AS APPLICATION"
cd ~/.local/share/applications
echo "[Desktop Entry]
Name=StartServer
Version=1.0
Type=Application
Exec=/home/iris-f1/pythonenvironmentsetup/startServer.sh
Icon=network-transmit-symbolic
Comment=Start IRis-F1 Server
Terminal=true" > startServer.desktop
chmod a+x startServer.desktop

echo "INSTALL NEW GUI"
echo "Enter desired version of GUI (if this step should be skipped, just press enter):"
read -p "Version (format example: 1.4.1): " guiVersion
if [ -z "$guiVersion" ]
then
    echo "Skipping GUI installation"
else
    echo "Installing new GUI"
    sudo snap remove irspectrometerclient
    cd ~/Downloads/
    wget https://find.irsweep.com/software/IRisGUI/irspectrometerclient_${guiVersion}_amd64.snap 
    sudo snap install --devmode irspectrometerclient_${guiVersion}_amd64.snap
fi

