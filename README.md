# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository is used to share the scripts to install a working python environment for the clients of the IRis-F1. The python environment is used to do post-processing steps shared in the IRis-F1UserScripts repo (https://bitbucket.org/irsweep/iris-f1userscripts/)

### How do I get set up? ###

* Summary of set up
Follow the steps in the knowledgebase articel: https://irsweep.atlassian.net/servicedesk/customer/kb/view/2132410369
